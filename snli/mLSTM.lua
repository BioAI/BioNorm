--[[
Copyright 2015 Singapore Management University (SMU). All Rights Reserved.

Permission to use, copy, modify and distribute this software and its documentation for purposes of research, teaching and general academic pursuits, without fee and without a signed licensing agreement, is hereby granted, provided that the above copyright statement, this paragraph and the following paragraph on disclaimer appear in all copies, modifications, and distributions.  Contact Singapore Management University, Intellectual Property Management Office at iie@smu.edu.sg, for commercial licensing opportunities.

This software is provided by the copyright holder and creator “as is” and any express or implied warranties, including, but not Limited to, the implied warranties of merchantability and fitness for a particular purpose are disclaimed.  In no event shall SMU or the creator be liable for any direct, indirect, incidental, special, exemplary or consequential damages, however caused arising in any way out of the use of this software.
]]
require 'cutorch'
require 'cunn'
local mLSTM = torch.class('seqmatchseq.mLSTM') 


function mLSTM:__init(config)
    self.mem_dim       = config.mem_dim       or 100  --state dimension 300
    self.att_dim       = config.att_dim       or self.mem_dim --attenion dimension 300
    self.fih_dim       = config.fih_dim       or self.mem_dim
    self.learning_rate = config.learning_rate or 0.002 --0.001
    self.emb_lr        = config.emb_lr        or 0  --embedding learning rate 0
    self.emb_partial   = config.emb_partial   or true --only update the non-pretrained embeddings
    self.batch_size    = config.batch_size    or 25 --number of sequences to train on in parallel
    self.reg           = config.reg           or 0 --regularize value
    self.emb_dim       = config.wvecDim       or 300 --word vector dimension
    self.task          = config.task          or 'snli'
    self.numWords      = config.numWords   
    self.dropoutP      = config.dropoutP      or 0  --dropoutP 0.3
    self.grad          = config.grad          or 'adamax'  --gradient descent method adamax
    self.num_classes   = config.num_classes   or 3

    self.best_score    = 0

    self.emb_vecs = Embedding(self.numWords, self.emb_dim) 
    self.emb_vecs.weight = tr:loadVacab2Emb(self.task):float() 
    if self.emb_partial then 
        self.emb_vecs.unUpdateVocab = tr:loadUnUpdateVocab(self.task) 
       
    end

    self.dropoutl = nn.Dropout(self.dropoutP)  
    self.dropoutr = nn.Dropout(self.dropoutP)  



    self.optim_state = { learningRate = self.learning_rate }
    self.criterion = nn.ClassNLLCriterion():cuda()  

    local lstm_config = {in_dim = self.emb_dim, mem_dim = self.mem_dim}  
    local wwatten_config = {att_dim = self.att_dim, mem_dim = self.mem_dim } 

    -- LSTM
    self.llstm = seqmatchseq.LSTM(lstm_config)  
    self.rlstm = seqmatchseq.LSTM(lstm_config)  
    self.rlstm:cuda()
    print('rlstm: type')
    print(self.rlstm:type())
    --attention model
    self.att_module = seqmatchseq.LSTMwwatten(wwatten_config)  
    --softmax
    self.soft_module = nn.Sequential():add(nn.Linear(self.mem_dim, self.num_classes)):add(nn.LogSoftMax()) 
    local modules = nn.Sequential():add(self.llstm):add(self.att_module):add(self.soft_module)
    --put modules cuda
    -- modules = nn.GPU(modules, 1)
    modules:cuda()
    print(modules:type())
    self.params, self.grad_params = modules:getParameters()
    self.best_params = self.params.new(self.params:size())
    share_params(self.rlstm, self.llstm)
end

function mLSTM:train(dataset)
    self.dropoutl:training()
    self.dropoutr:training()
    local indices = torch.randperm(dataset.size) 
    local zeros = torch.zeros(self.mem_dim)
    for i = 1, dataset.size, self.batch_size do 
        xlua.progress(i, dataset.size)
        local batch_size = math.min(i + self.batch_size - 1, dataset.size) - i + 1 

        local feval = function(x)
            self.grad_params:zero()
            self.gradWeight = {}
            local loss = 0
            for j = 1, batch_size do
                local idx = indices[i + j - 1]
                local lsent, rsent = dataset.lsents[idx], dataset.rsents[idx]  

                local linputs_emb = self.emb_vecs:forward(lsent)  
                local rinputs_emb = self.emb_vecs:forward(rsent)  

                local linputs = self.dropoutl:forward(linputs_emb) 
                local rinputs = self.dropoutr:forward(rinputs_emb)

                local cudaLinputs = linputs:cuda()
                local cudaRinputs = rinputs:cuda()

                self.llstm:forward(cudaLinputs)
                self.rlstm:forward(cudaRinputs)
                local lHinputs = self.llstm.hOutput  
                local rHinputs = self.rlstm.hOutput 

                local softInput = self.att_module:forward({lHinputs, rHinputs})   
                local output = self.soft_module:forward(softInput[2])  
               
                loss = loss + self.criterion:forward(output, dataset.labels[idx]) 
               

               
                local soft_grad = self.criterion:backward(output, dataset.labels[idx])
                local att_grad = self.soft_module:backward(softInput[2], soft_grad)

                local rep_grad = self:atten_backward(rsent, {lHinputs, rHinputs}, att_grad) 

                local linputs_grad = self.llstm:backward(linputs, rep_grad[1])
                local rinputs_grad = self.rlstm:backward(rinputs, rep_grad[2])

                if self.emb_lr ~= 0 then
                    linputs_emb_grad = self.dropoutl:backward(linputs_emb, linputs_grad)
                    rinputs_emb_grad = self.dropoutr:backward(rinputs_emb, rinputs_grad)
                    self.emb_vecs:backward(lsent, linputs_emb_grad)
                    self.emb_vecs:backward(rsent, rinputs_emb_grad)
                end

            end

            loss = loss / batch_size
           
            self.grad_params:div(batch_size)

           
            if self.reg ~= 0 then
                loss = loss + 0.5 * self.reg * self.params:norm() ^ 2
                self.grad_params:add(self.reg, self.params)
            end

            return loss, self.grad_params
        end
        optim[self.grad](feval, self.params, self.optim_state)
		if self.emb_lr ~= 0 then
        	self.emb_vecs:updateParameters(self.emb_lr)
		end
    end
    xlua.progress(dataset.size, dataset.size)
end

function mLSTM:atten_backward(rsent, inputs, att_grad)
    local grad = torch.zeros(rsent:nElement(), self.mem_dim):cuda()
    grad[rsent:nElement()] = att_grad
    local rep_grad = self.att_module:backward(inputs, grad)
    return rep_grad 
end

function mLSTM:predict(lsent, rsent)

    local linputs_emb = self.emb_vecs:forward(lsent)
    local rinputs_emb = self.emb_vecs:forward(rsent)

    local linputs = self.dropoutl:forward(linputs_emb)
    local rinputs = self.dropoutr:forward(rinputs_emb)

    local cudaLinputs = linputs:cuda()
    local cudaRinputs = rinputs:cuda()

    self.llstm:forward(cudaLinputs)
    self.rlstm:forward(cudaRinputs)
    local lHinputs = self.llstm.hOutput
    local rHinputs = self.rlstm.hOutput

    local softInput = self.att_module:forward({lHinputs, rHinputs})
    local output = self.soft_module:forward(softInput[2])

    self.att_module:forget()
    self.llstm:forget()
    self.rlstm:forget()

    local _,idx = torch.max(output,1)  
    return idx:cuda() 
end

function mLSTM:predict_dataset(dataset)
    self.dropoutl:evaluate()
    self.dropoutr:evaluate()
    local predictions = torch.Tensor(dataset.size):cuda()
    local correct = 0
    local tp = 0
    local tn = 0
    local fp = 0
    local fn = 0
    local measure = {}
    for i = 1, dataset.size do
        xlua.progress(i, dataset.size)
        local lsent, rsent = dataset.lsents[i], dataset.rsents[i]
       
        predictions[i] = self:predict(lsent, rsent)


        if predictions[i] == dataset.labels[i] then
            correct = correct + 1
            if dataset.labels[i] == 1 then
                tp = tp +1
            else
                tn = tn + 1
            end

        else
            if dataset.labels[i] == 1 then
                fn = fn + 1
            else
                fp = fp + 1
            end
        end
    end
    measure['precision'] = tp/(tp+fp)
    measure['recall'] = tp/(tp+fn)
    measure['f1'] = 2 * measure['precision'] * measure['recall'] / (measure['precision'] + measure['recall'])
    measure['accuracy'] = correct / dataset.size
    measure['specificity'] = tn/(tn+fp)

    print('F value is: '..measure['f1'])
    return measure
end

function mLSTM:predict_dataset_library(dataset)
    self.dropoutl:evaluate()
    self.dropoutr:evaluate()
    local predictions = torch.Tensor(dataset.size):cuda()
    local correct = 0
    local tp = 0
    local tn = 0
    local fp = 0
    local fn = 0
    local measure = {}
    for i = 1, dataset.size do
        xlua.progress(i, dataset.size)
        local lsent, rsent = dataset.lsents[i], dataset.rsents[i]
       
        predictions[i] = self:predict(lsent, rsent)

        if predictions[i] == 1 then
            print('same event')
        else
            print('different event')
        end
    end

    return measure
end

function mLSTM:predict_dataset_final(path, dataset, data)
    self.dropoutl:evaluate()
    self.dropoutr:evaluate()
    local predictions = torch.Tensor(dataset.size):cuda()
    local true_example_Path  = path ..'true_example_BioASQ.txt'
    local false_example_Path  = path ..'false_example_BioASQ.txt'
    local correct = 0
    local tp = 0
    local tn = 0
    local fp = 0
    local fn = 0
    local measure = {}

    local true_file = io.open(true_example_Path, 'a')
    local false_file = io.open(false_example_Path, 'a')


    for i = 1, dataset.size do
        xlua.progress(i, dataset.size)
        local lsent, rsent = dataset.lsents[i], dataset.rsents[i]
        predictions[i] = self:predict(lsent, rsent)
        if predictions[i] == dataset.labels[i] then
            correct = correct + 1

            if dataset.labels[i] == 1 then
                tp = tp +1
                true_file:write('the number of true positive is' .. '\t' .. tostring(i) ..'\t'..'the dataset is '..tostring(data)..'\n')
            else
                tn = tn + 1
                true_file:write('the number of true negative is' .. '\t' .. tostring(i) ..'\t'..'the dataset is '..tostring(data)..'\n')
            end

        else
            if dataset.labels[i] == 1 then
                fn = fn + 1
                false_file:write('the number of false negative is' .. '\t' .. tostring(i) ..'\t'..'the dataset is '..tostring(data)..'\n')
            else
                fp = fp + 1
                false_file:write('the number of false positive is' .. '\t' .. tostring(i) ..'\t'..'the dataset is '..tostring(data)..'\n')
            end
        end
    end
    measure['precision'] = tp/(tp+fp)
    measure['recall'] = tp/(tp+fn)
    measure['f1'] = 2 * measure['precision'] * measure['recall'] / (measure['precision'] + measure['recall'])
    measure['accuracy'] = correct / dataset.size
    measure['specificity'] = tn/(tn+fp)

    return measure
end



function mLSTM:save(path, config, result, epoch)  
    --('../trainedmodel/', opt, {recordDev, recordTest, recordTrain}, i)
    assert(string.sub(path,-1,-1)=='/')
    local paraPath     = path .. config.task .. config.expIdx 
    local paraBestPath = path .. config.task .. config.expIdx .. '_best'
    local recPath      = path .. config.task .. config.expIdx ..'Record.txt'

    local file = io.open(recPath, 'a')
    if epoch == 1 then
        for name, val in pairs(config) do
            file:write(name .. '\t' .. tostring(val) ..'\n')
        end
    end


    for i, val in pairs(result) do
        --file:write('accuracy:'..val['accuracy'] .. ', '..'precision:'..val['precision'] .. ', '..'recall:'.. val['recall'] .. ', '..'f1:'..val['f1']..', '..'specificity:'..val['specificity']..', ')
        if i == 1 then
            if epoch < config.max_epochs + 1 then
                file:write(config.task..': '..epoch..': ') 
                file:write('accuracy:'..val['accuracy'] .. ', '..'precision:'..val['precision'] .. ', '..'recall:'.. val['recall'] .. ', '..'f1:'..val['f1']..', '..'specificity:'..val['specificity']..', '..'\n')
                print('Dev: '..'accuracy:'..val['accuracy'] .. ', '..'precision:'..val['precision'] .. ', '..'recall:'.. val['recall'] .. ', '..'f1:'..val['f1']..', '..'specificity:'..val['specificity']..', ')
            else
                file:write('Dev accuracy:'..val['accuracy'] .. ', '..'precision:'..val['precision'] .. ', '..'recall:'.. val['recall'] .. ', '..'f1:'..val['f1']..', '..'specificity:'..val['specificity']..', '..'\n')
                print('Dev: '..'accuracy:'..val['accuracy'] .. ', '..'precision:'..val['precision'] .. ', '..'recall:'.. val['recall'] .. ', '..'f1:'..val['f1']..', '..'specificity:'..val['specificity']..', ')
            end
        elseif i == 2 then
            file:write('Test accuracy:'..val['accuracy'] .. ', '..'precision:'..val['precision'] .. ', '..'recall:'.. val['recall'] .. ', '..'f1:'..val['f1']..', '..'specificity:'..val['specificity']..', '..'\n')
            print('Test: '..'accuracy:'..val['accuracy'] .. ', '..'precision:'..val['precision'] .. ', '..'recall:'.. val['recall'] .. ', '..'f1:'..val['f1']..', '..'specificity:'..val['specificity']..', ')
        else
            file:write('Train accuracy:'..val['accuracy'] .. ', '..'precision:'..val['precision'] .. ', '..'recall:'.. val['recall'] .. ', '..'f1:'..val['f1']..', '..'specificity:'..val['specificity']..', '..'\n')
            print('Train: '..'accuracy:'..val['accuracy'] .. ', '..'precision:'..val['precision'] .. ', '..'recall:'.. val['recall'] .. ', '..'f1:'..val['f1']..', '..'specificity:'..val['specificity']..', ')
        end
    end
   

    file:close()
    if result[1]['accuracy'] > self.best_score then
        self.best_score  = result[1]['accuracy']
        self.best_params:copy(self.params)
        torch.save(paraBestPath, {params = self.params,config = config})    
    end
    torch.save(paraPath, {params = self.params, config = config})  
end

function mLSTM:load(path)
    local state = torch.load(path)
    self:__init(state.config)
    self.params:copy(state.params)
end
